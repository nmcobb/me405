import json, csv

with open('data.json') as json_file:
    data = json.load(json_file)

    with open('out.csv', 'w', newline='') as out_file:
        writer = csv.writer(out_file)
        titles = [item for sublist in list(map(lambda x: [x, ''], data.keys())) for item in sublist]
        writer.writerow(titles)
        for i in range(len(data[list(data.keys())[0]])):
            row = []
            for key in data.keys():
                row.append(data[key][i]['time'])
                row.append(data[key][i]['pos'])

            writer.writerow(row)