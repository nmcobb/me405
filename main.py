## @file main.py
#  Data collection for determining optimal kp constant
#  @package Lab3
#  @page ProportionalController
#  In order to tune my controller, I tested a serries of Kps and measured how the motor acted under each
#  The criteria which I investigated were the speed at which the motor reached the target point,
#  how close to the target point the motor was able to settle at, and whether the motor overshot the setpoint at all
#  @image html fig1.png "figure 1" width=50%
#  Here I observed that for most of the calibration, the calculated duty cycle was over 100%, so I decided to decrease the distance travled
#  to get a closer look at what was actually going on. I also noticed that .75, .5, and .25 seemed too high, as each of them made significant
#  overshots and had to recorrect to come back to the setpoint
#  @image html fig2.png "figure 2" width=50%
#  In the second calibration, I significantly decreased by Kp constants to prioritize accuracy over speed, and got more variation in my results.
#  From this data, I selected my Kp at .03
#  \htmlonly
#  <video width="540" height="310" controls>
#     <source src="IMG_2623.MOV" type="video/mp4">
#  </video>
#  \endhtmlonly
#  @author Miles Cobb
#  @date May 12, 2020

from Lab1 import MotorDriver
from Lab2 import Encoder
from Lab3 import Proportional_Controller
import pyb, utime

##
#  runs motor, encoder, and controller to test different kps
def main():
    ## motor driver object to control motor
    motor = MotorDriver(pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP),
                        pyb.Pin(pyb.Pin.cpu.B4),
                        pyb.Pin(pyb.Pin.cpu.B5),
                        pyb.Timer(3, freq = 20000))
    motor.enable()
    
    ## encoder to measure the current pos (using tim4)
    encoder = Encoder(4, 'B6', 'B7')
    
    
    ## how far we want the motor to move each itteration
    dist = 3000
    
    ## target pos for encoder
    setpoint = 0
    
    ## duty cycle for controller (ms)
    duty = 10
    
    ## user input
    u_in = None
    
    ## dict to store data entries
    data = {}
    while True:
        ## next kp
        kp = get_input()
        if kp is None:
            return
        
        ## controller to determine motor duty cycles
        control = Proportional_Controller(kp)
        
        setpoint += dist
        
        start_t = utime.ticks_ms()
        
        ## start point for run
        start_d = encoder.get_position()
        
        ## array to record data points
        run = []
        
        for i in range(2 / (duty / 1000)):
            ## current position
            pos = encoder.get_position()
            run.append({
                "time": utime.ticks_ms() - start_t,
                "pos": pos - start_d
            })
            ## next motor duty value
            next_duty = control.update(setpoint, pos)
            motor.set_duty(next_duty)
            utime.sleep_ms(duty)
        print('sleeping')
        data[kp] = run
        print(data)
    motor.disable()

## get kp constant from the user. This does not validate string.
#  intented value are >= 0, numeric values only
#  @return the input from the user
def get_input():
    val = input("Kp? (q or quit to exit) ")
    if(val == 'quit' or val == 'q'):
        return None
    return float(val)
        
if __name__ == '__main__':
    main()