## @file NUCLEO_script.py
#  @package Lab4
#  @page Final_Project
#  @section Nucleo
#  Recieves Face location data from RPI over I2C and moves motors base on position <br />
#  We used the motor driver class from lab 1, along with a proportional controller
#  to run the motors <br />
#  Utilized the <a href=classLab1_1_1MotorDriver.html> MotorDriver </a>
#  class to handle the motor PWMs <br />
#  A critical section of code
#  @code
# while True:
# try:
#     data = i2c.recv(3, timeout=5000)
# except OSError:
#     data = None
# if data is not None:
#     ## Recieved horizontal face position (ranges 0 - 100)
#     x = int(data[1])
#
#     ## Recieved vertical face position (ranges 0 - 100)
#     y = int(data[2])
#
#     # Send proportional commands (p = .1)
#     x_driver.set_duty((x - 50) * .1)
#     x_driver.set_duty((y - 50) * .1)
#
#     # detection rate of camera is too slow, so only make small steps
#     # then wait for next position
#     pyb.delay(200)
#     x_driver.set_duty(0)
#  @endcode 
#  
#  <a href=https://bitbucket.org/nmcobb/me405/src/master/NUCLEO_script.py>Full Code</a><br/>
#  NUCLEO_script.py <br/>
#  @author Miles Cobb
#  @author Miles Aikens
#  @date June 10, 2020

from Lab1 import MotorDriver

## i2c controller
i2c = pyb.I2C(1, pyb.I2C.SLAVE)
i2c.init(pyb.I2C.SLAVE, addr = 0x41)

## array to collect data
data = bytearray(200)

## horizontal motor controllerO\
x_driver = MotorDriver(pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP),
                       pyb.Pin(pyb.Pin.cpu.B4),
                       pyb.Pin(pyb.Pin.cpu.B5),
                       pyb.Timer(3, freq = 20000))

## Vertical motor controller
y_driver = MotorDriver(pyb.Pin(pyb.Pin.cpu.C1, pyb.Pin.OUT_PP),
                       pyb.Pin(pyb.Pin.cpu.A0),
                       pyb.Pin(pyb.Pin.cpu.A1),
                       pyb.Timer(5, freq = 20000))

x_driver.enable()
y_driver.enable()

while True:
    try:
        data = i2c.recv(3, timeout=5000)
    except OSError:
        data = None
    if data is not None:
        ## Recieved horizontal face position (ranges 0 - 100)
        x = int(data[1])

        ## Recieved vertical face position (ranges 0 - 100)
        y = int(data[2])

        # Send proportional commands (p = .1)
        x_driver.set_duty((x - 50) * .1)
        x_driver.set_duty((y - 50) * .1)

        # detection rate of camera is too slow, so only make small steps
        # then wait for next position
        pyb.delay(200)
        x_driver.set_duty(0)