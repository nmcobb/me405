import pyb
from Lab1 import MotorDriver
i2c = pyb.I2C(1, pyb.I2C.SLAVE)
i2c.init(pyb.I2C.SLAVE, addr = 0x41)
data = bytearray(200)

x_driver = MotorDriver(pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP),
                         pyb.Pin(pyb.Pin.cpu.B4),
                         pyb.Pin(pyb.Pin.cpu.B5),
                         pyb.Timer(3, freq = 20000))

y_driver = MotorDriver(pyb.Pin(pyb.Pin.cpu.C1, pyb.Pin.OUT_PP),
                         pyb.Pin(pyb.Pin.cpu.A0),
                         pyb.Pin(pyb.Pin.cpu.A1),
                         pyb.Timer(5, freq = 20000))

x_driver.enable()
y_driver.enable()

while True:
    try:
        data = i2c.recv(3, timeout=5000)
    except OSError:
        data = None
    if data is not None:
        x = int(data[1])
        y = int(data[2])
        
        print(x, y)
        if (x > 55):
            x_driver.set_duty(-9)
        
        if (x < 45):
            x_driver.set_duty(9)
        
        if (y > 55):
            y_driver.set_duty(-9)
        
        if (y < 45):
            y_driver.set_duty(9)
            
        pyb.delay(100)
        x_driver.set_duty(0)
        y_driver.set_duty(0)
        