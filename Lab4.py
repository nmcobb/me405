## @file Lab4.py
#  Class for IMU encapsulation
#  @package Lab4
#  @page IMU
#  To test the accuracy of my IMU I printed out a protractor and used it to measure the change in angles at different positions. I measured the yaw at 0, 90, and 180 on the protractor
#  and recorded the following yaws from the IMU respectively: 0.24, 90.72, 181.1. While there is still some error here, I decided this was a reasonable error margin given my test setup
#  <a href=https://bitbucket.org/nmcobb/me405/src/a0e677c511d91e28bbd8cdc4975e9f2a0d38f511/Lab4.py>CODE</a> 
#  \htmlonly
#  <video width="540" height="310" controls>
#     <source src="IMG_2623.MOV" type="video/mp4">
#  </video>
#  \endhtmlonly
#  @author Miles Cobb
#  @date May 12, 2020

from pyb import I2C
from ustruct import unpack
from utime import sleep_ms

#  byte value for NDOF status
NDOF = 0b00001100
DISABLED = 0b00001011

#  disabled byte status
DEVICE_ADDR = 0x28

#  address of mode register
MODE_REGISTER = 0x3D

#  addresses of orientation registers (EUL)
OREINTATION_REGISTERS = {
    "PITCH": 0x1E,
    "YAW": 0x1A,
    "ROLL": 0x1C
}

#  addresses of angular velocity register addresses
VELOCITY_REGISTERS = {
    "X": 0x14,
    "Y": 0x16,
    "Z": 0x18
}

#  calibration status register address
CALIBRATION_STAT_REG = 0x35

## Encapsulates IMU functionality
#  This fetches angular velocities and orientation in EUL
class IMU:
    ## Creates an IMU reader, uses i2c 1
    def __init__(self):
        self.i2c = I2C(1, I2C.MASTER)
        pass
    
    ## Enables the IMU
    def enable(self):
        self.i2c.mem_write(NDOF, DEVICE_ADDR, MODE_REGISTER)
    
    ## Disables the IMU
    def disable(self):
        self.i2c.mem_write(DISABLED, DEVICE_ADDR, MODE_REGISTER)
    
    ## Gets the IMU's orientation registers
    #  @return a tuple containing orientations (PITCH, YAW, ROLL)
    def get_orientation(self):
        arr = [0, 0, 0]
        for (i, key) in enumerate(OREINTATION_REGISTERS.keys()):
            # Read MSB and LSB from registers at the same time, and unpack into signed short
            # / 16 to get degrees
            arr[i] = unpack("<h", self.i2c.mem_read(2, DEVICE_ADDR, OREINTATION_REGISTERS[key]))[0] / 16
        return tuple(arr)
    
    ## Gets the IMU's angular velocities
    #  @return a tuple containing angular velocities (X, Y, Z)
    def get_velocities(self):
        arr = [0, 0, 0]
        for (i, key) in enumerate(VELOCITY_REGISTERS.keys()):
            # Read MSB and LSB from registers at the same time, and unpack into signed short
            # / 16 to get proper units
            arr[i] = unpack("<h", self.i2c.mem_read(2, DEVICE_ADDR, VELOCITY_REGISTERS[key]))[0] / 16
        return tuple(arr)
    
    ## Fetches the IMU's calibration
    #  @return the IMU's calibration
    def get_calibration(self):
        #read from register
        value = ord(self.i2c.mem_read(1, DEVICE_ADDR, CALIBRATION_STAT_REG))
        
        #isolate each set of 2 bits, 3=0b11 and right shifting
        return (
            (value >> 6) & 3,
            (value >> 4) & 3,
            (value >> 2) & 3,
            value & 3
        )
    
if __name__  == '__main__':
    i = IMU()
    i.enable()
    while True:
        sleep_ms(500)
        print("ORIENTATION", i.get_orientation(), "VELOCITY", i.get_velocities(), "CALIBRATION", i.get_calibration())
