## @file mainpage.py
#  @author Miles Cobb
#  @mainpage
#
#  Main page for ME 405 (Cobb)
#
#  This documenation includes details for all labs in ME-405
#
#
#  @section lab1 Motor Driver
#  ME-405 Lab One: Encapsulating DC PWM
#  The motor driver encapsulates the functionality of PWM timers. For usage see MotorDriver.MotorDriver
#  this driver is able to enable and disable the motor, as well as run it forwards and backwards at varying speeds <br>
#  <a href=https://bitbucket.org/nmcobb/me405/src/master/Lab1/>Lab 1 Code</a> 
#
#  @section lab2 Encoder
#  ME-405 Lab Two: Encapsulating Encoders
#  Encapsulates the encoder to get the motor's position. Also negates overflow and underflow <br>
#  <a href=https://bitbucket.org/nmcobb/me405/src/master/Lab2.py>Lab 2 Code</a> 
#
#  @section lab3 Controller
#  ME-405 Lab Two: Proportional Controller tuning
#  Created proportional controller, and wrote code for tuning <br>
#  <a href=https://bitbucket.org/nmcobb/me405/src/a0e677c511d91e28bbd8cdc4975e9f2a0d38f511/Lab3.py>Controller Code</a> 
#  <a href=https://bitbucket.org/nmcobb/me405/src/a0e677c511d91e28bbd8cdc4975e9f2a0d38f511/main.py>Tuning Code</a> 
#
#  @date May 12, 2020
#