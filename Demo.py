## @file Demo.py
#  @page Final_Project
#  @section Project Overview
#  Our project was to create a face tracking robot. In order to do this, we used
#  a Raspberry Pi and camera to collect images, and isolated faces in the capture image.
#  We then transmit the position of a face to the Nucleo over I2C, which is responsible 
#  for controling a vertical and horizontal actuator (proportional controllers)
#  to adjust the camera's position such that the face is in the center of the
#  frame. We used a 3D printer to build a frame for this project.
#
#  @section DEMO
#  \htmlonly
#  <video width="540" height="310" controls>
#     <source src="IMG_2676.MOV" type="video/mp4">
#  </video> <br />
#  \endhtmlonly
#  In this demo video, my my faces is directly to the right of my phone which 
#  I am using to record this video. As you can see, the camera pivots to follow
#  me as i move slightly left and right. Due to the speed of the RPI, the refresh
#  rate is quite slow, so that's why the camera takes a moment to respond to motion.
#  Additionally, due to some hardware difficulties (cables) we were not able to 
#  increase our mobility further<br />
#  @image html model.png "figure 1" width=50%
#  3D Model of our parts printed in PLA
#  encorporate a larger range of motion.