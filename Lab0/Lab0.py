## @file Lab0.py
#  Brief doc for Lab0.py
#
#  Detailed doc for encoder.py 
#
#  @author Miles Cobb
#
#  @copyright NA
#
#  @date April 18, 2020
#
#  @package Lab0
#  Brief doc for the fib lab
#
#  Detailed doc for the fib module
#
#  @author Miles Cobb
#
#  @copyright NA
#
#  @date April 18, 2020


## Calculates a fib number at index x
#
#  Uses dynamic programming to avoid repetitive computation
#
#  @param idx The index of fib number to calculate
def fib (idx):
    print ('Calculating Fibonacci number at '
           'index n = {:}.'.format(idx))
    vals = {0: 0, 1: 1}
    for i in range(2, idx + 1):
        vals[i] = vals[i - 1] + vals[i - 2]
        
    return vals[idx]

if __name__ == '__main__':
    while True:
        ## User input
        val = input("Index? (q or quit to exit) ")
        print(fib(int(val)))