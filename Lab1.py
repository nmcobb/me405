## @file Lab1.py
#  Class for pwm control over two motors
#  @package MotorDriver
#  encapsulates functionality of pwm timers, allows for enable, disable
#  forwards, and backwards control
#  @author Miles Cobb
#  @date April 22, 2020
#  @package Lab1
import pyb
from math import fabs

## Encapsulates pwm functionality
class MotorDriver:
    ## Creates a motor driver object with specified pins and timer
    #  @param EN_pin A pyb.Pin object to use as the enable pin.
    #  @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
    #  @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
    #  @param timer   A pyb.Timer object to use for PWM generation on IN1_pin and IN2_pin.
    def __init__(self, EN_pin, IN1_pin, IN2_pin, timer):
        ## enable pin
        self.EN_pin = EN_pin
        
        ## in pin for IN1
        self.IN1_pin = IN1_pin
        ## in pin for IN2
        self.IN2_pin = IN2_pin
        ## timer for PWM objects
        self.timer = timer
        ## backwards pwm channel
        self.forward_ch = self.timer.channel(1, pyb.Timer.PWM, pin=self.IN1_pin)
        ## forwards pwm channel
        self.back_ch = self.timer.channel(2, pyb.Timer.PWM, pin=self.IN2_pin)
        
    ## Enables the EN_pin
    def enable(self):
        print('Enabling Motor')
        self.EN_pin.value(1)
        print(self.EN_pin.value())
        
    ## Disables the EN_pin
    def disable(self):
        print('Disabling Motor')
        self.EN_pin.value(False)
        print(self.EN_pin.value())
    
    ## Sets the duty cycle of the motor, postive values indicate forwards
    #  negative values indicate backwards
    #  @param duty A signed integer (the duty cycle) for the PWM signal
    def set_duty(self, duty):
        if duty == 0:
            # set both pins to low
            self.forward_ch.pulse_width_percent(0)
            self.back_ch.pulse_width_percent(0)
        elif duty > 0:
            self.forward_ch.pulse_width_percent(fabs(duty))
            self.back_ch.pulse_width_percent(0)
        else:
            self.forward_ch.pulse_width_percent(0)
            self.back_ch.pulse_width_percent(fabs(duty))
        
if __name__ == '__main__':
    driver = MotorDriver(pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP),
                         pyb.Pin(pyb.Pin.cpu.B4),
                         pyb.Pin(pyb.Pin.cpu.B5),
                         pyb.Timer(3, freq = 20000))
    driver.enable()
    print("FORWARD slow")
    driver.set_duty(20)
    pyb.delay(2000)
    print("STOP")
    driver.set_duty(0)
    pyb.delay(2000)
    print("BACKWARD slow")
    driver.set_duty(-20)
    pyb.delay(2000)
    
    print("FORWARD faster")
    driver.set_duty(40)
    pyb.delay(2000)
    print("STOP")
    driver.set_duty(0)
    pyb.delay(2000)
    print("BACKWARD faster")
    driver.set_duty(-40)
    pyb.delay(2000)
    
    print("STOPPING")
    driver.disable()