## @file RPI_script.py
#  @package Lab4
#  @page Final_Project
#  @section RPI
#  Captures images from the raspicam, runs a haar based detecter, and reports
#  found faces to nucleo over I2C <br />
#  Camera Resolution: 1920 x 1080 <br />
#  System frequency: 2hz <br />
#  Future improvements: <br />
#  1). More specific hardware will accelerate the detection, currently this 
#      limits our frequency heavily <br />
#  2). Distinguish specific faces to track one specific face <br />
#  3). Create our own training data that is more specific to our methods <br />
#  RPI_script.py<br/>
#  <a href=https://bitbucket.org/nmcobb/me405/src/master/RPI_script.py>Full Code</a> <br/>
#  @image html example.jpg "figure 2" width=50%
#  Detection is done utilizing a Haar Based Classifier, using the pretrained 
#  OpenCV model for detecting faces. We capture the image, flip it vertically
#  (camera is mounted upside down), convert to grayscale, then run the classifier
#  We chose a Haar based detector because it is independent of scale.
#  Figure 2 is an exapmle of a detected face in an image. <br />
#  Face detection code
#  @code
# def find_faces():
#     camera.capture(rawCapture, format="bgr")
#     img = cv2.flip(rawCapture.array, 0)
#     img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
#     faces = face_cascade.detectMultiScale(img_gray, 1.3, 5)
#     if(len(faces) == 0):
#         faces = [[img.shape[1] / 2.0, img.shape[0] / 2.0]]
#     else:
#         print('face found')
#     x_percent = faces[0][0] * 100. / img.shape[1]
#     y_percent = faces[0][1] * 100. / img.shape[0]
#     rawCapture.truncate(0)
#     return [int(x_percent), int(y_percent)]
#  @endcode
#  @author Miles Cobb
#  @author Miles Aikens
#  @date June 10, 2020

import cv2, time, os, smbus, struct
from picamera.array import PiRGBArray
from picamera import PiCamera
camera = PiCamera()
time.sleep(0.1)
rawCapture = PiRGBArray(camera)
face_cascade = cv2.CascadeClassifier('./opencv/data/haarcascades/haarcascade_frontalface_default.xml')

## Captures an image from the raspi cam, and detects faces
#  @return x, y where x and y are respectively the height and width percentage of the first detected face (default 50,50)
def find_faces():
    camera.capture(rawCapture, format="bgr")
    img = cv2.flip(rawCapture.array, 0)
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(img_gray, 1.3, 5)
    if(len(faces) == 0):
        faces = [[img.shape[1] / 2.0, img.shape[0] / 2.0]]
    else:
        print('face found')
    x_percent = faces[0][0] * 100. / img.shape[1]
    y_percent = faces[0][1] * 100. / img.shape[0]
    rawCapture.truncate(0)
    return [int(x_percent), int(y_percent)]

# display system info
print(os.uname())
bus = smbus.SMBus(1)

## I2C address of Slave
i2c_address = 0x41
i2c_cmd = 0x00

# send welcome message at start-up
# bus.write_i2c_block_data(i2c_address, i2c_cmd, [0, 0])
# loop to send message
while True:
    faces = find_faces()
    print(faces)
    bus.write_i2c_block_data(i2c_address, i2c_cmd, faces)