## @file Lab3.py
#  Proportional Controller
#  @package Lab3
#  Class for proportional controller
#  @author Miles Cobb
#  @date May 12, 2020
import pyb

## Encapsulates Proportional Controller
class Proportional_Controller:
    ## Creates an proportional controller
    #  @param kp kp constant to use
    def __init__(self, kp):
        self.kp = kp
    
    ## Creates an proportional controller
    #  @param reference the setpoint for the controller
    #  @param measured the measurement taken to be fed back into the controller
    #  @return the calculated next input for the motor
    def update(self, reference, measured):
        return (reference - measured) * self.kp
        
#testing
if __name__ == '__main__':
    motor = MotorDriver(pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP),
                        pyb.Pin(pyb.Pin.cpu.B4),
                        pyb.Pin(pyb.Pin.cpu.B5),
                        pyb.Timer(3, freq = 20000))
    motor.enable()
    encoder = Encoder(4, 'B6', 'B7')
    
    control = Proportional_Controller(.05)
    
    target = 10000
    while True:
        pos = encoder.get_position()
        next_duty = control.update(target, pos)
        print(next_duty, pos)
        motor.set_duty(next_duty)
        pyb.delay(10)