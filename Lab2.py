## @file Lab2.py
#  Working with encoders
#  @package Lab2
#  Class for encoder functionality, and testing
#  This will not overflow.
#  @author Miles Cobb
#  @date May 6, 2020
import pyb

## Encapsulates encoder
class Encoder:
    ## Creates an encoder object with specified pins and timer
    #  @param timer_no an integer to use for the Encoder timer (use 4)
    #  @param pin_a_no a string representing the pin number for pin A
    #  @param pin_b_no a string representing the pin number for pin B    
    def __init__(self, timer_no, pin_a_no, pin_b_no):
        ## the most recent reading taken by the encoder
        self.enc_pos = 0

        ## the current position of the encoder generated using deltas
        self.pos = 0

        ## the previous reading recorded by the encoder
        self.last = 0
        
        ## encoder pin A
        a = pyb.Pin(pin_a_no, pyb.Pin.AF_PP, pyb.Pin.IN)
        
        ## encoder pin B
        b = pyb.Pin(pin_b_no, pyb.Pin.AF_PP, pyb.Pin.IN)
    
        ## timer object to collect encoder data
        self.timer = pyb.Timer(timer_no, prescaler=0, period=65535)
        
        # Add channels a and b
        self.timer.channel(1, pyb.Timer.ENC_AB, pin=a)
        self.timer.channel(1, pyb.Timer.ENC_AB, pin=b)
        
    ## Updates the encoder's position using the delta between the last two measurements
    def update(self):
        self.last = self.enc_pos
        self.enc_pos = self.timer.counter()
        self.pos += self.get_delta()
        
    ## Gets the current position of the encoder. This will not overflow or underflow.
    #  @return (float) the current position of the encoder
    def get_position(self):
        self.update()
        return self.pos
    
    ## Gets the delta position from the encoder between the last two measurements
    #  This is normalized to avoid overflow and overflow
    #  @return (float) the delta between last two measureuments
    def get_delta(self):
        delta = self.enc_pos - self.last
        if delta > 65535 / 2:
            return delta - 65535
        if delta < -65535 / 2:
            return delta + 65535
        
        return delta

if __name__ == '__main__':
    ## TESTING HERE
    #  Run motor by hand to test

    ## first encoder for testing
    t = Encoder(8, 'C6', 'C7')

    ## Second encoder for testing
    t2 = Encoder(4, 'B6', 'B7')

    for i in range(1000):
        t.update()
        t2.update()
        print(t.get_position(), t2.get_position())
        pyb.delay(10)
